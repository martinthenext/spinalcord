source("LoadData.R")

library("gplots")

drawHeatmap <- function(data, type.str, side.str, display=TRUE, cluster.segments=FALSE) {
  if (display) {
    x11()
  }
  data.mtx <- getScoreMtx(data, type.str, side.str, na.rm=TRUE)
  head(data.mtx)
  if (cluster.segments) {
    heatmap.2(data.mtx, dendrogram="column", trace="none", Rowv=FALSE)
  } else {
    heatmap.2(data.mtx, dendrogram="row", trace="none", Colv=FALSE)
  }
}

score.avg.colnames.motor <- paste('motor', motor.segments, 'avg', sep='.')
score.avg.colnames.sensory <- paste('sensory', sensory.segments, 'avg', sep='.')

heatmap.2(as.matrix(data.avg.scores.lesion[, score.avg.colnames.motor]), dendrogram="row", trace="none", Colv=FALSE)
heatmap.2(as.matrix(data.avg.scores.lesion[, score.avg.colnames.sensory]), dendrogram="row", trace="none", Colv=FALSE)
